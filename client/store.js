/**
 * External Dependencies
 */
import { createStore, compose } from 'redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { browserHistory } from 'react-router';


// Import root reducer
import rootReducer from './reducers/index';

// Import initial data (using json files in `data` dir but this could come from an API)
import comments from './data/comments';
import posts from './data/posts';

// Create an object for the default data
const defaultState = {
	posts,
	comments
};

// Create any enhancers with compose from Redux.
// Below checks for Chrome's Redux Dev-Tools extension and returns it.
// If it doesn't exist, it just returns the store.
const enhancers = compose(
	window.devToolsExtension ? window.devToolsExtension() : f => f
);

// Create Store
const store = createStore( rootReducer, defaultState, enhancers );

// Sync browser history and export to make avbailable to other modules (not as default)
export const history = syncHistoryWithStore( browserHistory, store );

// Export store as default
export default store;
