// Increment Likes
export const increment = ( index ) => {
	return {
		type: 'INCREMENT_LIKES',
		//index: index
		index
	};
};

// Add Comment
export const addComment = ( postId, author, comment ) => {
	console.log( "Dispatching ADD_COMMENT" );
	return {
		type: 'ADD_COMMENT',
		postId,
		author,
		comment
	};
};

// Remove Comment
export const removeComment = ( postId, index ) => {
	return {
		type: 'REMOVE_COMMENT',
		index,
		postId
	};
};
