/**
 * External Dependencies
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

/**
 * Internal Dependencies
 */
import posts from './posts';
import comments from './comments';

const rootReducer = combineReducers( { posts, comments, routing: routerReducer } );

export default rootReducer;
