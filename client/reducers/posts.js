// A reducer takes in two things:
// 1. The action (info about what happened)
// 2. A copy of the current state

const posts = ( state = [], action ) => {
	switch( action.type ) {
		case 'INCREMENT_LIKES':
			// Return the updated state
			//console.log( 'Incrementing likes!' );
			//console.log( state, action );
			const i = action.index;
			return [
				...state.slice( 0, i ), // Everything up to the one we're updating
				{ ...state[i], likes: state[i].likes + 1 }, // Creating/passing new object for the one we're updating, using an Object Spread - same as Object.assign
				...state.slice( i + 1 ), // Everything after the one we're updating
			];
		default:
			return state;
	}
};

export default posts;
