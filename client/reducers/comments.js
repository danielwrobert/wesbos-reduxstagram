// A reducer takes in two things:
// 1. The action (info about what happened)
// 2. A copy of the current state

const postComments = ( state = [], action ) => {
	switch ( action.type ) {
		case 'ADD_COMMENT':
			// Return the new state with the new comment
			return [
				...state,
				{
					user: action.author,
					text: action.comment
				}
			]
		case 'REMOVE_COMMENT':
			console.log( 'Removing a comment' );
			// Return the state without the deleted comment
			return [
				// From the start up until the one we want to delete
				...state.slice( 0, action.index ),
				// Everything after the deleted comment
				...state.slice( action.index + 1 )
			];
		default:
			return state;
	}
	return state;
};

const comments = ( state = [], action ) => {
	if ( typeof action.postId !== 'undefined' ) {
		return {
			// Take everything from the current state
			...state,
			// Overwrite this post with a new one
			[action.postId]: postComments( state[action.postId], action )
		}
	}
	return state;
};

export default comments;
