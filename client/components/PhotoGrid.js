/**
 * External Dependencies
 */
import React, { Component } from 'react';
import Photo from './Photo';


class PhotoGrid extends Component {
	render() {
		return (
			<div className="photo-grid">
				{
					//JSON.stringify( this.props.posts, null, '' )
					this.props.posts.map( ( post, i ) => {
						return <Photo { ...this.props } key={ i } index={ i } post={ post } />;
					} )
				}
			</div>
		);
	}
}

export default PhotoGrid;
