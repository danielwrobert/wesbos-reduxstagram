/**
 * External Dependencies
 */
import React, { Component } from 'react';

class Comments extends Component {
	// Note: Could use fat arrow function in the renderComment and handleSubmit
	// function calls but that feature is not set up in the Babel presets for this
	// project. So I am using `.bind` in the render function, instead.
	renderComment( comment, i ) {
		return (
			<div className="comment" key={ i }>
				<p>
					<strong>{ comment.user }</strong>
					{ comment.text }
					<button className="remove-comment" onClick={ () => this.props.removeComment( this.props.params.postId, i ) }>&times;</button>
				</p>
			</div>
		)
	}

	handleSubmit( e ) {
		e.preventDefault();

		const { postId } = this.props.params;
		const author = this.refs.author.value;
		const comment = this.refs.comment.value;

		this.props.addComment( postId, author, comment );
		this.refs.commentForm.reset();
	}

	render() {
		return (
			<div className="comments">
				{ this.props.postComments.map( this.renderComment.bind( this ) ) }
				<form action="" ref="commentForm" className="comment-form" onSubmit={ this.handleSubmit.bind( this ) }>
					<input type="text" ref="author" placeholder="author" />
					<input type="text" ref="comment" placeholder="comment" />
					<input type="submit" hidden />
				</form>
			</div>
		);
	}
}

export default Comments;
