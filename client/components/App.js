/**
 * External Dependencies
 */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


/**
 * Internal Dependencies
 */
import * as actionCreators from '../actions/actioncreators';
import Main from './Main';


const mapStateToProps = state => {
	return {
		posts: state.posts,
		comments: state.comments
	};
};

const mapDispatchToProps = dispatch => {
	return bindActionCreators( actionCreators, dispatch );
};

const App = connect( mapStateToProps, mapDispatchToProps )( Main );

export default App;
